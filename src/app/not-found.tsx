'use client';

import { routing } from '@/i18n/routing';
import { redirect, usePathname } from 'next/navigation';

export default function NotFound() {
  const pathname = usePathname();
  const defaultLocale = routing.defaultLocale;

  // Add a locale prefix to show a localized not found page
  redirect(`/${defaultLocale}${pathname}`);
}
