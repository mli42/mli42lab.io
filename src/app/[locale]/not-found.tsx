import Button from '@/components/Atoms/Button';
import SvgHouse from '@/components/Svg/SvgHouse';
import { Link } from '@/plugins/i18n';
import { getTranslations } from 'next-intl/server';
import Image from 'next/image';

export default async function NotFound() {
  const t = await getTranslations('notFound');

  return (
    <div className="flex h-screen flex-col items-center justify-center gap-y-4">
      <div className="flex flex-row items-center gap-x-4">
        <Image
          src="/red-cat.webp"
          width="50"
          height="50"
          alt="Sitting red cat by Lina Chekhovich"
          title="Sitting red cat by Lina Chekhovich"
        />

        <div className="flex flex-col">
          <p>{t('title')}</p>
          <p>{t('description')}</p>
        </div>
      </div>
      <Link href="/">
        <Button className="flex flex-row items-center gap-x-2">
          <p>{t('returnHome')}</p>
          <SvgHouse className="h-6 w-6" />
        </Button>
      </Link>
    </div>
  );
}
