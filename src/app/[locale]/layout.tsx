import './globals.css';
import './waves.css';

import Navbar from '@/components/Organisms/Navbar';
import { sourceCodePro } from '@/fonts/fonts';
import { routing } from '@/i18n/routing';
import { Locale } from '@/types/i18n';
import { Analytics } from '@vercel/analytics/react';
import { SpeedInsights } from '@vercel/speed-insights/next';
import classNames from 'classnames';
import type { Metadata } from 'next';
import { NextIntlClientProvider } from 'next-intl';
import {
  getMessages,
  getTranslations,
  setRequestLocale,
} from 'next-intl/server';
import { notFound } from 'next/navigation';
import Script from 'next/script';
import Provider from './themeProvider';

type MetadataProps = {
  params: Promise<{ locale: string }>;
};

export async function generateMetadata({
  params,
}: MetadataProps): Promise<Metadata> {
  let locale = (await params).locale;

  if (!routing.locales.includes(locale as Locale)) {
    locale = routing.defaultLocale;
  }

  const t = await getTranslations({ locale, namespace: 'metadata' });
  const title = 'Marc Li';
  const description = t('description');
  const siteURL = 'https://marcli.com';

  return {
    metadataBase: new URL(siteURL),
    title: {
      default: title,
      template: `%s | ${title}`,
    },
    description,
    manifest: '/manifest.json',
    openGraph: {
      type: 'website',
      url: siteURL,
      title,
      description,
      siteName: title,
      images: [{ url: '/meta-preview.png' }],
    },
    twitter: {
      card: 'summary_large_image',
    },
  };
}

export function generateStaticParams() {
  return routing.locales.map((locale) => ({ locale }));
}

type LayoutProps = {
  children: React.ReactNode;
  params: Promise<Record<string, string>>;
};

export default async function Layout({ children, params }: LayoutProps) {
  const locale = (await params).locale;
  if (!routing.locales.includes(locale as Locale)) notFound();
  setRequestLocale(locale);

  const messages = await getMessages();

  return (
    <html lang={locale} suppressHydrationWarning>
      <body
        className={classNames(
          'text-dark dark:bg-dark dark:text-gray-300',
          sourceCodePro.variable
        )}
      >
        <NextIntlClientProvider messages={messages}>
          <Provider>
            <Navbar />
            {children}
          </Provider>
        </NextIntlClientProvider>

        {/* Google tag (gtag.js) */}
        <Script
          src="https://www.googletagmanager.com/gtag/js?id=G-6N9TQ7R4S6"
          strategy="lazyOnload"
        />
        <Script id="gtag-manager" strategy="lazyOnload">
          {`window.dataLayer = window.dataLayer || []; function gtag() { dataLayer.push(arguments); }; gtag('js', new Date()); gtag('config', 'G-6N9TQ7R4S6');`}
        </Script>

        {/* Vercel Analytics */}
        <Analytics />
        <SpeedInsights />
      </body>
    </html>
  );
}
