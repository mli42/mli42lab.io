import classNames from 'classnames';
import React, { FC } from 'react';

interface Props {
  className?: string;
}

const SvgWave: FC<Props> = ({ className }) => {
  return (
    <svg
      className={classNames('waves', className)}
      viewBox="0 24 150 28"
      preserveAspectRatio="none"
      shapeRendering="auto"
    >
      <defs>
        <path
          id="gentle-wave"
          fill="currentColor"
          d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"
        />
      </defs>
      <g className="parallax">
        <use xlinkHref="#gentle-wave" x="48" y="0" fillOpacity="0.7" />
        <use xlinkHref="#gentle-wave" x="48" y="3" fillOpacity="0.5" />
        <use xlinkHref="#gentle-wave" x="48" y="5" fillOpacity="0.3" />
        <use xlinkHref="#gentle-wave" x="48" y="7" />
      </g>
    </svg>
  );
};

export default SvgWave;
