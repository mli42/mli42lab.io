# portfolio

Using these tools:

<div align="center">
  <a href="https://nextjs.org/" target="_blank" rel="noreferrer">
    <img src="https://img.shields.io/badge/next-%2320232a.svg?style=for-the-badge&logo=next.js" alt="Next" />
  </a>
  <a href="https://www.typescriptlang.org/" target="_blank" rel="noreferrer">
    <img src="https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white" alt="typescript" />
  </a>
  <a href="https://tailwindcss.com/" target="_blank" rel="noreferrer">
    <img src="https://img.shields.io/badge/tailwindcss-%2338B2AC.svg?style=for-the-badge&logo=tailwind-css&logoColor=white" alt="tailwind" />
  </a>
  <!-- <a href="https://mantine.dev/" target="_blank" rel="noreferrer">
    <img src="https://img.shields.io/badge/mantine-4D98EC.svg?style=for-the-badge&logo=mantine&logoColor=white" alt="mantine" />
  </a> -->
  <br />
  <a href="https://zustand-demo.pmnd.rs/" target="_blank" rel="noreferrer">
    <img src="https://img.shields.io/badge/zustand-%23593d88.svg?style=for-the-badge&logoColor=white" alt="Zustand" />
  </a>
  <a href="https://eslint.org/" target="_blank" rel="noreferrer">
    <img src="https://img.shields.io/badge/ESLint-4B3263?style=for-the-badge&logo=eslint&logoColor=white" alt="ESLint" />
  </a>
  <a href="https://prettier.io/" target="_blank" rel="noreferrer">
    <img src="https://img.shields.io/badge/Prettier-182025?style=for-the-badge&logo=prettier" alt="Prettier" />
  </a>
  <br />
  <a href="https://next-intl-docs.vercel.app/" target="_blank" rel="noreferrer">
    <img src="https://img.shields.io/badge/next%20intl-26A69A?style=for-the-badge&logo=i18next&logoColor=white" alt="next-intl" />
  </a>
  <a href="https://www.docker.com/" target="_blank" rel="noreferrer">
    <img src="https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white" alt="docker" />
  </a>
  <a href="https://typicode.github.io/husky/" target="_blank" rel="noreferrer">
    <img src="https://img.shields.io/badge/husky-182025?style=for-the-badge" alt="Husky" />
  </a>
</div>

And *React Atomic Design*:

<div align="center">
  <img src="https://andelav4prod.wpengine.com/wp-content/uploads/2019/10/Screenshot-2019-10-25-at-2.31.27-PM.png" alt="React Atomic Design Schema" />
</div>

## Available Scripts in `Makefile`

This app uses `Docker` and `docker-compose`, thus we use a Makefile:

- `all` (up & logs)
- `up` (start dev server)
- `exec` (exec shell in docker container)
- `build` (build for production)
- `deploy` (push prod build)
- `re` (fclean & all)
- `reload` (down & all)
- `logs` (follow docker container logs)
- `stop` (stop container)
- `down` (down container)
- `ps` (print container status)
- `fclean` (remove images, dist, node_modules)
